
# VERZE 2.0
import random as rn

class GamePlayer:
	
	#Encapsulates a player properties
	

	def __init__(self, _id):
		self._id = _id
		# initial dummy rank -1
		self.rank = -1
		# starting position for every player is 1
		self.position = 1

	def set_position(self, pos):
		self.position = pos

	def set_rank(self, rank):
		self.rank = rank

	def get_pos(self):
		return self.position

	def get_rank(self):
		return self.rank


class MovingEntity:
	
	
	

	def __init__(self, end_pos=None):
		self.end_pos = end_pos
		self.desc = None

	def set_description(self, desc):
		self.desc = None

	def get_end_pos(self):
		if self.end_pos is None:
			raise Exception("no_end_position_defined")
		return self.end_pos


class Snake(MovingEntity):
	#Snake entity

	def __init__(self, end_pos=None):
		super(Snake, self).__init__(end_pos)
		self.desc = "Bit by Snake"


class Ladder(MovingEntity):
	#Ladder entity

	def __init__(self, end_pos=None):
		super(Ladder, self).__init__(end_pos)
		self.desc = "Climbed Ladder"


class Board:

	def __init__(self, size):
		self.size = size
		# {pos:moving_entity} to save space
		self.board = {}

	def get_size(self):
		return self.size

	def set_moving_entity(self, pos, moving_entity):
		# set moving entity to pos
		self.board[pos] = moving_entity

	def get_next_pos(self, player_pos):
		# get next pos given a specific position player is on
		if player_pos > self.size:
			return player_pos
		if player_pos not in self.board:
			return player_pos
		print(f'{self.board[player_pos].desc} at {player_pos}')
		return self.board[player_pos].get_end_pos()

	def at_last_pos(self, pos):
		if pos == self.size:
			return True
		return False


class Dice:
	def __init__(self, sides):
		self.sides = sides

	def roll(self):
		# return random number between 1 to sides
		import random
		ans = random.randrange(1, self.sides + 1)
		return ans


class Game:

	def __init__(self):
	# game board object
		self.board = None
		# game dice object
		self.dice = None
		# list of game player objects
		self.players = []
		# curr turn
		self.turn = 0
		self.winner = None
		# last rank achieved
		self.last_rank = 0
		# no of consecutive six in one turn, resets every turn
		self.consecutive_six = 0

	def initialize_game(self, board: Board, dice_sides, players):
		"""
	#Initialize game using board, dice and players
        """
		self.board = board
		self.dice = Dice(dice_sides)
		self.players = [GamePlayer(i) for i in range(players)]

	def can_play(self):
		if self.last_rank != len(self.players):
			return True
		return False

	def get_next_player(self):
		"""
	#Return curr_turn player but if it has already won/completed game , return next player which is still active
        """
		while True:
			# if rank is -1 , player is still active so return
			if self.players[self.turn].get_rank() == -1:
				return self.players[self.turn]
			# check next player
			self.turn = (self.turn + 1) % len(self.players)

	def move_player(self, curr_player, next_pos):
		# Move player to next_pos
		curr_player.set_position(next_pos)
		if self.board.at_last_pos(curr_player.get_pos()):
			# if at last position set rank
			curr_player.set_rank(self.last_rank + 1)
			self.last_rank += 1

	def can_move(self, curr_player, to_move_pos):
		# check if player can move or not ie. between board bound
		if to_move_pos <= self.board.get_size() and curr_player.get_rank() == -1:
			return True
		return False

	def change_turn(self, dice_result):
        # change player turn basis dice result.
        # if it's six, do not change .
        # if it's three consecutive sixes or not a six, change
		self.consecutive_six = 0 if dice_result != 6 else self.consecutive_six + 1
		if dice_result != 6 or self.consecutive_six == 3:
			if self.consecutive_six == 3:
				print("Changing turn due to 3 consecutive sixes")
			self.turn = (self.turn + 1) % len(self.players)
		else:
			print(f"One more turn for player {self.turn+1} after rolling 6")
            # Check if the player landed on a Snake or Ladder after rolling a 6
			curr_player = self.players[self.turn]
			next_pos = self.board.get_next_pos(curr_player.get_pos())
			if self.can_move(curr_player, next_pos):
				self.move_player(curr_player, next_pos)
				print(f"Player {self.turn+1} rolled 6 but stays in place due to Snake or Ladder")

	def play(self):
		while self.can_play():
			curr_player = self.get_next_player()
			player_input = input(
				f"Player {self.turn+1}, Press enter to roll the dice")
			dice_result = self.dice.roll()
			print(f'dice_result: {dice_result}')
			_next_pos = self.board.get_next_pos(curr_player.get_pos() + dice_result)
			if self.can_move(curr_player, _next_pos):
				self.move_player(curr_player, _next_pos)
			self.change_turn(dice_result)
			self.print_game_state()
		self.print_game_result()

	def print_game_state(self):
		# Print state of game after every turn
		print("")
		print('-------------game state-------------')
		for ix, _p in enumerate(self.players):
			print(f'Player: {ix+1} is at position {_p.get_pos()}')
		print('-------------game state-------------\n')

	def print_game_result(self):
		# Print final game result with ranks of each player
		print('-------------final result-------------')
		for _p in sorted(self.players, key=lambda x: x.get_rank()):
			print(f'Player: {_p._id+1}, Rank: {_p.get_rank()}')


def get_number_of_players():
    while True:
        try:
            num_players = int(input("Enter the number of players: "))
            if num_players > 0:
                return num_players
            else:
                print("The number of players must be a positive number.")
        except ValueError:
            print("Enter a valid integer for the number of players.")

def sample_run():
	board = Board(100)
	board.set_moving_entity(16, Snake(6))
	board.set_moving_entity(46, Snake(25))
	board.set_moving_entity(49, Snake(11))
	board.set_moving_entity(62, Snake(19))
	board.set_moving_entity(64, Snake(60))
	board.set_moving_entity(74, Snake(53))
	board.set_moving_entity(89, Snake(68))
	board.set_moving_entity(92, Snake(88))
	board.set_moving_entity(95, Snake(75))
	board.set_moving_entity(99, Snake(80))
	board.set_moving_entity(2, Ladder(38))
	board.set_moving_entity(7, Ladder(14))
	board.set_moving_entity(8, Ladder(31))
	board.set_moving_entity(15, Ladder(26))
	board.set_moving_entity(21, Ladder(42))
	board.set_moving_entity(28, Ladder(84))
	board.set_moving_entity(36, Ladder(44))
	board.set_moving_entity(51, Ladder(67))
	board.set_moving_entity(71, Ladder(91))
	board.set_moving_entity(78, Ladder(98))
	board.set_moving_entity(87, Ladder(94))
	num_players = get_number_of_players()
	game = Game()
	game.initialize_game(board, 6, num_players)
	game.play()

sample_run()







"""

# VERZE 2.1 #

# DOBRÉ, FUNGUJE, ALE NENÍ SČÍTÁNÍ HODŮ A ŘEŠENÍ POSTAVIČEK NA STEJNÉM MÍSTĚ

import random as rn

class GamePlayer:
    def __init__(self, _id):
        self._id = _id
        self.rank = -1
        self.position = 1

    def set_position(self, pos):
        self.position = pos

    def set_rank(self, rank):
        self.rank = rank

    def get_pos(self):
        return self.position

    def get_rank(self):
        return self.rank


class MovingEntity:
    def __init__(self, end_pos=None):
        self.end_pos = end_pos
        self.desc = None

    def set_description(self, desc):
        self.desc = None

    def get_end_pos(self):
        if self.end_pos is None:
            raise Exception("no_end_position_defined")
        return self.end_pos


class Snake(MovingEntity):
    def __init__(self, end_pos=None):
        super(Snake, self).__init__(end_pos)
        self.desc = "Bit by Snake"


class Ladder(MovingEntity):
    def __init__(self, end_pos=None):
        super(Ladder, self).__init__(end_pos)
        self.desc = "Climbed Ladder"


class Board:
    def __init__(self, size):
        self.size = size
        self.board = {}

    def get_size(self):
        return self.size

    def set_moving_entity(self, pos, moving_entity):
        self.board[pos] = moving_entity

    def get_next_pos(self, player_pos):
        if player_pos > self.size:
            return player_pos
        if player_pos not in self.board:
            return player_pos
        print(f'{self.board[player_pos].desc} at {player_pos}')
        return self.board[player_pos].get_end_pos()

    def at_last_pos(self, pos):
        return pos == self.size


class Dice:
    def __init__(self, sides):
        self.sides = sides

    def roll(self):
        import random
        ans = random.randrange(1, self.sides + 1)
        return ans


class Game:
    def __init__(self):
        self.board = None
        self.dice = None
        self.players = []
        self.turn = 0
        self.winner = None
        self.last_rank = 0
        self.consecutive_six = 0

    def initialize_game(self, board: Board, dice_sides, players):
        self.board = board
        self.dice = Dice(dice_sides)
        self.players = [GamePlayer(i) for i in range(players)]

    def can_play(self):
        return self.last_rank != len(self.players)

    def get_next_player(self):
        while True:
            if self.players[self.turn].get_rank() == -1:
                return self.players[self.turn]
            self.turn = (self.turn + 1) % len(self.players)

    def move_player(self, curr_player, next_pos):
        if next_pos in self.board.board:
            moving_entity = self.board.board[next_pos]
            if isinstance(moving_entity, GamePlayer):
                print(f"Player {curr_player._id+1} landed on a position with another player. Moving the other player back.")
                other_player = moving_entity
                other_player_position = other_player.get_pos()
                if other_player_position != 1 and other_player_position != self.board.size:
                    other_player_end_pos = self.board.get_next_pos(other_player_position - 1)
                    other_player.set_position(other_player_end_pos)
                    print(f"Player {other_player._id+1} moved back to position {other_player.get_pos()} due to collision.")
        curr_player.set_position(next_pos)
        if self.board.at_last_pos(curr_player.get_pos()):
            curr_player.set_rank(self.last_rank + 1)
            self.last_rank += 1

    def can_move(self, curr_player, to_move_pos):
        return to_move_pos <= self.board.get_size() and curr_player.get_rank() == -1

    def change_turn(self, dice_result):
        self.consecutive_six = 0 if dice_result != 6 else self.consecutive_six + 1
        if dice_result != 6 or self.consecutive_six == 3:
            if self.consecutive_six == 3:
                print("Changing turn due to 3 consecutive sixes")
            self.turn = (self.turn + 1) % len(self.players)
        else:
            print(f"One more turn for player {self.turn+1} after rolling 6")
            curr_player = self.players[self.turn]
            next_pos = self.board.get_next_pos(curr_player.get_pos())
            if self.can_move(curr_player, next_pos):
                self.move_player(curr_player, next_pos)
                print(f"Player {self.turn+1} rolled 6 but stays in place due to Snake or Ladder")

    def play(self):
        while self.can_play():
            curr_player = self.get_next_player()
            player_input = input(f"Player {self.turn+1}, Press enter to roll the dice")
            dice_result = self.dice.roll()
            print(f'dice_result: {dice_result}')
            _next_pos = self.board.get_next_pos(curr_player.get_pos() + dice_result)
            if self.can_move(curr_player, _next_pos):
                self.move_player(curr_player, _next_pos)
            self.change_turn(dice_result)
            self.print_game_state()
        self.print_game_result()

    def print_game_state(self):
        print("")
        print('-------------game state-------------')
        for ix, _p in enumerate(self.players):
            print(f'Player: {ix+1} is at position {_p.get_pos()}')
        print('-------------game state-------------\n')

    def print_game_result(self):
        print('-------------final result-------------')
        for _p in sorted(self.players, key=lambda x: x.get_rank()):
            print(f'Player: {_p._id+1}, Rank: {_p.get_rank()}')


def get_number_of_players():
    while True:
        try:
            num_players = int(input("Enter the number of players: "))
            if num_players > 0:
                return num_players
            else:
                print("The number of players must be a positive number.")
        except ValueError:
            print("Enter a valid integer for the number of players.")

def sample_run():
    board = Board(100)
    board.set_moving_entity(16, Snake(6))
    board.set_moving_entity(46, Snake(25))
    board.set_moving_entity(49, Snake(11))
    board.set_moving_entity(62, Snake(19))
    board.set_moving_entity(64, Snake(60))
    board.set_moving_entity(74, Snake(53))
    board.set_moving_entity(89, Snake(68))
    board.set_moving_entity(92, Snake(88))
    board.set_moving_entity(95, Snake(75))
    board.set_moving_entity(99, Snake(80))
    board.set_moving_entity(2, Ladder(38))
    board.set_moving_entity(7, Ladder(14))
    board.set_moving_entity(8, Ladder(31))
    board.set_moving_entity(15, Ladder(26))
    board.set_moving_entity(21, Ladder(42))
    board.set_moving_entity(28, Ladder(84))
    board.set_moving_entity(36, Ladder(44))
    board.set_moving_entity(51, Ladder(67))
    board.set_moving_entity(71, Ladder(91))
    board.set_moving_entity(78, Ladder(98))
    board.set_moving_entity(87, Ladder(94))
    num_players = get_number_of_players()
    game = Game()
    game.initialize_game(board, 6, num_players)
    game.play()

sample_run()
"""
